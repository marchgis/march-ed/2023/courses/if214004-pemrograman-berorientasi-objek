# Business Pitch

## Kriteria Penilaian
1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
    - Use case user
    - Use case manajemen perusahaan
    - Use case direksi perusahaan (dashboard, monitoring, analisis)
2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
5. Mampu menunjukkan dan menjelaskan konektivitas ke database
6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
    - Pemahaman terkait Web Service
        - Artikel: 
            - [Apa itu Web service - Dicoding](https://www.dicoding.com/blog/apa-itu-web-service/)
            - [Apa itu Web service - JagoanHosting](https://www.jagoanhosting.com/blog/web-service-adalah/)
            - [Pengertian Web service - DewaWeb](https://www.dewaweb.com/blog/apa-itu-web-service/)
            - [Web Service - Revou](https://revou.co/kosakata/web-service)
        - Video:
            - [API Web Services Beginner Tutorial 1 - Introduction - What is a Web Service](https://www.youtube.com/watch?v=oTzNRv6X51o)
            - [What is a Web Service? And why is it called a Web Service?](https://www.youtube.com/watch?v=e3bz4dxoUII)
    - Contoh library untuk membuat web service RESTful API:
        - [Java - Spring](https://spring.io/guides/gs/rest-service/)
        - [Java - Spark Java](https://sparkjava.com/)
        - [Kotlin - Spark Java](https://sparkjava.com/)
        - [TypeScript - Express](https://blog.logrocket.com/how-to-set-up-node-typescript-express/)
        - [Python - FastAPI](https://fastapi.tiangolo.com/lo/)
        - [PHP - Slim](https://www.slimframework.com/)
        - [Ruby - Sinatra](https://sinatrarb.com/)
7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
    - Contoh library GUI OOP:
        - [Java - Swing](https://www.javatpoint.com/java-swing)
        - [Java - Android SDK](https://developer.android.com/training/basics/firstapp)
        - [C++ - QT](https://wiki.qt.io/Qt_for_Beginners)
        - [C++ - GTK](https://www.gtk.org/docs/language-bindings/cpp)
        - [Dart - Flutter](https://flutter.dev/)
        - [TypeScript - React](https://create-react-app.dev/docs/adding-typescript/) [Contoh project](https://gitlab.com/marchgis/march-ed/2023/courses/sample-projects/dana-web-client)
9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
10. BONUS !!! Mendemonstrasikan penggunaan Machine Learning
