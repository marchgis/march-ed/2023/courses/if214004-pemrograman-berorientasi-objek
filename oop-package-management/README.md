# OOP Package Management
Di berbagai bahasa pemrograman berorientasi objek, terdapat fitur pengelolaan package yang memudahkan menginstall, menghapus, dan mengkonfigurasi package berisi berbagai kebutuhan pemrograman seperti konektivitas database, web, pemrosesan data, pengolahan file, user interface, hingga kecerdasan artifisial.

Beberapa diantaranya:
Bahasa | Package manager
---|---
Python | Pip [🔎 cari](https://pypi.org/)
Ruby | Gem [🔎 cari](https://rubygems.org/)
Java<br />Kotlin | Maven [🔎 cari](https://central.sonatype.com/)<br />Gradle [🔎 cari](https://central.sonatype.com/)
C++ | Vcpkg [🔎 cari](https://vcpkg.io/en/packages.html)<br />Conan [🔎 cari](https://conan.io/center/)
PHP | Composer [🔎 cari](https://packagist.org/)
Typescript | NPM [🔎 cari](https://docs.npmjs.com/cli/v7/commands/npm-search)<br />Yarn [🔎 cari](https://yarnpkg.com/)
Dart | Pub [🔎 cari](https://pub.dev/packages)

# Contoh Package di Tiap Bahasa

## Java
### Web framework
- [Spring Boot](https://spring.io/projects/spring-boot/)
- [Jooby](https://jooby.io/)
- [Spark Java](https://sparkjava.com/)
### Database management
- [Postgres JDBC Driver](https://mvnrepository.com/artifact/org.postgresql/postgresql)
- [MySQL Connector](https://mvnrepository.com/artifact/mysql/mysql-connector-java)
- [MongoDB Driver](https://mvnrepository.com/artifact/org.mongodb/mongodb-driver-sync)
### UI
- Java Swing - built in
- [JavaFX](https://openjfx.io/)
- [Apache Pivot](https://pivot.apache.org/)
### HTTP client
- [Google HTTP Client](https://mvnrepository.com/artifact/com.google.http-client/google-http-client)
- [Async HTTP Client](https://mvnrepository.com/artifact/com.ning/async-http-client)

