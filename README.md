# IF214004 - Pemrograman Berorientasi Objek

## Learning Outcome Outline
- Mampu menjelaskan pemanfaatan OOP dalam industri
- Mampu menjelaskan dan mendemonstrasikan konsep dasar OOP
    **- Class, Object, Attribute and Method**
    - Basic Object Oriented Programming (OOP)
        - Class
        - Object
        - Attribute
        - Method
    - Attribute
        - Data structure
    - Method
        - Parameter
        - Return
        - Overloading
        - Overriding
        - Control
        - Loop
    - Inheritance
    - Encapsulation
    - Abstraction
    - Polymorphism
    - Interface Implementation
    - Relation
        - Association
        - Aggregation
        - Composition
- Mampu membaca dokumentasi dan mendemonstrasikan penggunaan library bahasa pemrograman berbasis paradigma OOP
    - Big Data : Apache Spark Java
    - Game : Godot C#
    - Machine Learning : scikit-learn Python
    - Frontend Development : Browser API Javascript
    - Mobile Development : Flutter Dart, Android SDK Java
    - Backend Development : Fiber Go, Laravel PHP

## Pertemuan

No | Learning Outcome | Materi & Asesmen
---|---|---
1 | Pengantar OOP | [Materi](./pertemuan-1)
2 | Dasar OOP (Class, Object, Attribute and Method) | [Materi](./pertemuan-2)
3 | Class (Attribute, Modifier, Constructor, Static) | [Materi](./pertemuan-3)
4 | Method (Algoritma dan Struktur Data) | [Materi](./pertemuan-4)
5 | 4 Pilar OOP Abstraction (Abstraction Class, Interface) dan Encapsulation | [Materi](./pertemuan-5)
6 | 4 Pilar OOP Inheritance dan Polymorphism (Overloading, Overriding) | [Materi](./pertemuan-6)
7 | Relation (Association, Aggregation, Composition) dan Packaging | [Materi](./pertemuan-7)
8 | UTS |
9 | Class Diagram | [Materi](./pertemuan-8) |
10 | SOLID Design Principle | [Materi](./pertemuan-10)
11 | Design Pattern part 1 | [Materi](./pertemuan-11-12-13)
12 | Design Pattern part 2 | [Materi](./pertemuan-11-12-13)
13 | Design Pattern part 3 | [Materi](./pertemuan-11-12-13)
14 | Implementsai dan integrasi OOP pada database dan web service | [Materi](./pertemuan-14)
15 | Implementasi OOP pada UI dan HTTP client | [Materi](./pertemuan-15)
16 | UAS |

## Tools
- Alice 3
- Greenfoot
- Java
- Browser

## Referensi

### Buku
#### Dasar Coding
- Head First Learn to Code: A Learner’s Guide to Coding and Computational Thinking (2018)
- Clean Code

#### Basic OOP
- Timothy A. Budd - An Introduction to Object-Oriented Programming-Pearson (2021)
- Vaskaran Sarcar - Interactive Object-Oriented Programming in Java (2nd Ed)-Apress (2020)
- The Object-Oriented Thought Process (2019)
- Head First Object-Oriented Analysis and Design (2006)

#### OOP Design Pattern
- Vaskaran Sarcar - Java Design Patterns_ A Hands-On Experience with Real-World Examples-Apress (2022)
- Head First Design Patterns: Building Extensible and Maintainable Object-Oriented Software (2021)


### Materi
- [Java OOP - Tutorialspoint](https://www.w3schools.com/java/java_oop.asp)
- [C++ OOP - Tutorialspoint](https://www.w3schools.com/cpp/cpp_oop.asp)
- [C# OOP - W3schools](https://www.w3schools.com/cs/cs_oop.php)
- [Kotlin OOP - W3schools](https://www.w3schools.com/kotlin/kotlin_oop.php)
- [Javascript OOP - Mozilla](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_programming)
- [Github top 100 stars](https://github.com/EvanLi/Github-Ranking/blob/master/Top100/Top-100-stars.md)


### Instalasi Tools
- 

### Kurikulum
- 
