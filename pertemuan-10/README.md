# SOLID Design Principle

## 1. Single Responsibility Principle (SRP)

### Ide Utama
- Setiap Class / modul
- Hanya boleh memiliki satu tanggung jawab / satu alasan berubah

### Contoh
Contoh baik
```java
class PanitiaKonsumsi {
    void beliKonsumsi() {}
    void ambilDanaKonsumsi() {}
    void hitungDanaKonsumsi() {}
}
```

Contoh buruk
```java
class PanitiaKonsumsi {
    void beliKonsumsi() {}
    void ambilDanaKonsumsi() {}
    void hitungDanaKonsumsi() {}
    void buatSpandukPublikasi() {} // Wow apa ini guys ! melanggar SRP
    void bikinDekorasi() {} // Wow apa ini guys ! melanggar SRP
}
```

### Referensi
- [Sumber C-SharpCorner.com](https://www.c-sharpcorner.com/article/solid-single-responsibility-principle-with-c-sharp/)
- [Sumber DotNetTutorials.net](https://dotnettutorials.net/lesson/single-responsibility-principle/)

## 2. Open-Closed Principle

## 3. Liskov Substitution Principle

## 4. Interface Segregation Principle

## 5. Dependency Inversion Principle

## Referensi
### Materi
- [SOLID The First Five Principles of Object Oriented Design - DigitalOcean](https://www.digitalocean.com/community/conceptual-articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design)
- [SOLID principles - Baeldung](https://www.baeldung.com/solid-principles)
### Cheatsheet
- [Download SOLID Cheatsheet - www.monterail.com](https://www.monterail.com/hubfs/PDF%20content/SOLID_cheatsheet.pdf)

### Video
- [Software Design - Introduction to SOLID Principles in 8 Minutes](https://www.youtube.com/watch?v=yxf2spbpTSw)
