# Class Diagram

## Apa itu Class Diagram ???
- Diagram yang memodelkan struktur statik dari sebuah sistem
- Menggambarkan:
    - Class
    - Attributes: Atribut dari Class tersebut
    - Operations: Aksi yang dapat dilakukan oleh Class tersebut
- Dapat digunakan untuk:
    - Pemodelan kelas pada pemrograman berorientasi objek
    - Pemodelan proses bisnis

## Mengapa Class Diagram digunakan ?
- Mengkomunikasikan struktur statik dari sistem
- Memudahkan perancangan dan analisis struktur statik dari sebuah sistem
- Menunjukkan tanggung jawab dari setiap komponen sistem

## Komponen Class Diagram
1. Class name: Nama Class
2. Class attributes: Atribut Class, tipe data, modifier
3. Class operations: Operasi Class, tipe data output, parameter operasi, modifier

## Modifier
Tanda | Deskripsi
--- | ---
\+ | Modifier public
\- | Modifier private
\# | Modifier protected

## Pembuatan Class Diagram Pada File MD Menggunakan Mermaid.js
Semua contoh Class Diagram pada file MD ini dibuat menggunakan Mermaid.js dapat dilihat kodenya dengan melihat [source code dari file MD ini](?plain=1)

## Contoh Pembuatan Class
```mermaid
classDiagram
    class Mahasiswa {
        -String nim
        -String kodeJurusan
        +getNim()
        +getKodeJurusan()
        +daftarUlang(semester, tahun)
        +lihatTranskrip(semester, tahun)
    }
```

## Bentuk Relasi Antar Class
```mermaid
classDiagram
classA --|> classB : Inheritance
classC --* classD : Composition
classE --o classF : Aggregation
classG --> classH : Association
classI -- classJ : Link(Solid)
classK ..> classL : Dependency
classM ..|> classN : Realization
classO .. classP : Link(Dashed)
```

## Contoh Ralasi Antar Class
```mermaid
classDiagram
    class Manusia {
        #String nama
        +getNama()
    }

    class Mahasiswa {
        -String nim
        -String kodeJurusan
        +getNIM()
        +getKodeJurusan()
        +daftarUlang(semester, tahun)
        +lihatTranskrip(semester, tahun)
    }

    class Dosen {
        -String nip
        -String kodeJurusan
        -Mahasiswa List~mahasiswaBimbingan~
        +getNIP()
        +getKodeJurusan()
    }

    Manusia <|-- Mahasiswa : Inheritance
    Manusia <|-- Dosen : Inheritance
    Dosen --> Mahasiswa : Membimbing
```

## Challenge
Menambahkan Class Diagram menggunakan Mermaid.js pada file MD job interview nya

## Referensi
- [UML (Unified Modeling Language) - utsa.edu](http://www.cs.utsa.edu/~cs3443/uml/uml.html)
- [UML - Class Diagram - Tutorialspoint](https://www.tutorialspoint.com/uml/uml_class_diagram.htm)
- [What is Class Diagram?](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-class-diagram/)
- [UML - Behavioral Diagram vs Structural Diagram](https://www.visual-paradigm.com/guide/uml-unified-modeling-language/behavior-vs-structural-diagram/)
- [The UML 2 class diagram - IBM](https://developer.ibm.com/articles/the-class-diagram/)
- [UML Class Diagram Tutorial - Lucidchart](https://www.lucidchart.com/pages/uml-class-diagram)
- [UML Class Diagram - Javatpoint](https://www.javatpoint.com/uml-class-diagram)
- [Class Diagram - mermaid.js](https://mermaid.js.org/syntax/classDiagram.html)
